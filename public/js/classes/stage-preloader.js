Wert.StagePreloader = function() {
	Wert.StagePreloader.super.constructor.call(this);
}

Extend(Wert.StagePreloader, Wert.Stage);

Wert.StagePreloader.prototype.Create = function() {
	Wert.StagePreloader.super.Create.call(this);
	
	PIXI.Loader.shared
	.add("Atlas", "./img/atlas.json")
	.add("Tileset", "./maps/underwater.json")
	.add("Level1", "./maps/level1.json")
	.add("Level2", "./maps/level2.json")
	.add("Level3", "./maps/level3.json")
	.add("Level4", "./maps/level4.json")
	.add("Level5", "./maps/level5.json")
	.add("Level6", "./maps/level6.json")
	.add("MainMenu", "./maps/main-menu.json")
	.add("LevelSelect", "./maps/level-select.json")
	.add("BackgroundMusic", "./audio/background.mp3")
	.add("Pop", "./audio/pop.wav")
	.add("Fire", "./audio/fire.wav")
	.add("Death", "./audio/death.wav")
	.load(this.PreloadingFinished);
};

Wert.StagePreloader.prototype.Update = function() {
	Wert.StagePreloader.super.Update.call(this);
};

Wert.StagePreloader.prototype.Resize = function(newSize) {	
	Wert.StagePreloader.super.Resize.call(this, newSize);
};

Wert.StagePreloader.prototype.PreloadingFinished = function() {
	PIXI.Loader.shared.resources["BackgroundMusic"].data.volume = 0.25;
	PIXI.Loader.shared.resources["BackgroundMusic"].data.loop = true;
	PIXI.Loader.shared.resources["Fire"].data.volume = 0.25;
	PIXI.Loader.shared.resources["Pop"].data.volume = 0.25;
	//PIXI.Loader.shared.resources["Death"].data.volume = 0.25;
			
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;

	Wert.BubbleHero.prototype.SmileFaceTexture = sheet.textures['faces/smile'];
	Wert.BubbleHero.prototype.BigSmileFaceTexture = sheet.textures['faces/smile_big'];
	Wert.BubbleHero.prototype.SurprisedFaceTexture = sheet.textures['faces/surprised'];
	Wert.BubbleHero.prototype.EyelidsTexture = sheet.textures['faces/expresionless_blink'];
	Wert.BubbleHero.prototype.AngryFaceTexture = sheet.textures['faces/angry'];
	Wert.BubbleHero.prototype.AshamedFaceTexture = sheet.textures['faces/ashamed'];
	Wert.BubbleHero.prototype.ConfusedFaceTexture = sheet.textures['faces/confused'];

	Wert.BubbleMom.prototype.SmileFaceTexture = sheet.textures['faces/smile_mom'];
	Wert.BubbleMom.prototype.SurprisedFaceTexture = sheet.textures['faces/surprised_mom'];
	Wert.BubbleMom.prototype.EyelidsTexture = sheet.textures['faces/expresionless_blink'];

	Wert.BubbleChild.prototype.SmileFaceTexture = sheet.textures['faces/smile_child'];
	Wert.BubbleChild.prototype.EyelidsTexture = sheet.textures['faces/expresionless_blink'];
	
	Wert.SwitchStage(new Wert.StageMainMenu());
};
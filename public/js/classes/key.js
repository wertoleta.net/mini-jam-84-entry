Wert.Key = function() {
	Wert.Key.super.constructor.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	let texture = sheet.textures['key-blue'];
	let sprite = new PIXI.Sprite(texture);
	sprite.scale.set(2);
	sprite.anchor.set(0.5);
	this.addChild(sprite);
	
	this.radius = 32;
	
}

Extend(Wert.Key, Wert.Item);


Wert.Key.prototype.Update = function() {
	Wert.Key.super.Update.call(this);
};
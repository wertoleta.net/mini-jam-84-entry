Wert.StageGame = function(levelNumber) {
	Wert.StageGame.super.constructor.call(this);
	
	if (levelNumber > 0)
	{
		this.levelNumber = levelNumber;
		this.map = PIXI.Loader.shared.resources["Level" + levelNumber].data;
		this.map.size = {
			width: Math.max(this.map.width * this.map.tilewidth, Wert.ViewportSize.min.width),
			height: Math.max(this.map.height * this.map.tileheight, Wert.ViewportSize.min.height),
		};
	}
	this.bubbles = [];
	this.lines = [];
	this.items = [];
	this.forceTo = {x: 0, y: 0};
	this.innerLayer = new PIXI.Container();
	this.textLayer = new PIXI.Container();
}

Extend(Wert.StageGame, Wert.Stage);

Wert.StageGame.prototype.bubbles = null;
Wert.StageGame.prototype.lines = null;
Wert.StageGame.prototype.items = null;
Wert.StageGame.prototype.hero = null
Wert.StageGame.prototype.background = null;
Wert.StageGame.prototype.midground = null;
Wert.StageGame.prototype.levelNumber = 0;
Wert.StageGame.prototype.map = null;
Wert.StageGame.prototype.buttonOffset = 8;
Wert.StageGame.prototype.selectedBubble = null;
Wert.StageGame.prototype.forceTo = null;
Wert.StageGame.prototype.innerLayer = null;
Wert.StageGame.prototype.textLayer = null;
Wert.StageGame.prototype.restartButton = null;
Wert.StageGame.prototype.homeButton = null;
Wert.StageGame.prototype.soundButton = null;


Wert.StageGame.prototype.Create = function() {
	Wert.StageGame.super.Create.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	this.background = new PIXI.TilingSprite(sheet.textures['background'], Wert.ViewportSize.min.width, Wert.ViewportSize.min.height);
	this.midground = new PIXI.TilingSprite(sheet.textures['midground'], Wert.ViewportSize.min.width, Wert.ViewportSize.min.height);
	
	this.restartButton = new Wert.Button(sheet.textures['buttons/restart'], function(){
		Wert.SwitchStage(new Wert.StageGame(Wert.CurrentStage.levelNumber))
	});
	this.restartButton.normalScale = 0.5;
	
	this.homeButton = new Wert.Button(sheet.textures['buttons/home'], function(){
		Wert.SwitchStage(new Wert.StageMainMenu())
	});
	this.homeButton.normalScale = 0.5;
	
	this.soundButton = new Wert.Button(sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'], function(){
		if (!Wert.SoundPlayed)
		{
			PIXI.Loader.shared.resources["BackgroundMusic"].data.play();
			Wert.SoundPlayed = true;
			Wert.IsMuted = false;
		}
		else
		{
			Wert.IsMuted = !Wert.IsMuted;
			PIXI.Loader.shared.resources["BackgroundMusic"].data.muted = Wert.IsMuted;
		}
		this.sprite.texture = sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'];
	});
	this.soundButton.normalScale = 0.5;
	
	this.addChild(this.background);	
	this.addChild(this.midground);	
	this.addChild(this.innerLayer);	
	
	this.FillStage(this.map);
	
	this.addChild(this.textLayer);	
	this.addChild(this.restartButton);	
	this.addChild(this.homeButton);	
	this.addChild(this.soundButton);	
};

Wert.StageGame.prototype.Update = function() {
	Wert.StageGame.super.Update.call(this);
	
	if (this.selectedBubble != null) {
		let forceVector = new Victor(this.mouse.x - this.selectedBubble.position.x, this.mouse.y - this.selectedBubble.position.y);
		let forceAmount = Math.min(10.0, forceVector.length() / 5.0); 
		this.selectedBubble.velocity.add(forceVector.normalize().multiply(new Victor(forceAmount, forceAmount)));
	}
	
	let collisionVector = new Victor(0, 0);
	let vector1, vector2;
	for (let bubbleIndex1 = 0; bubbleIndex1 < this.bubbles.length; bubbleIndex1++)
	{
		var bubble1 = this.bubbles[bubbleIndex1];
		for (let bubbleIndex2 = bubbleIndex1 + 1; bubbleIndex2 < this.bubbles.length; bubbleIndex2++)
		{
			let bubble2 = this.bubbles[bubbleIndex2];
			collisionVector.x = bubble2.position.x - bubble1.position.x;
			collisionVector.y = bubble2.position.y - bubble1.position.y;
			if (collisionVector.length() < bubble1.radius + bubble2.radius) 
			{
				let emergeSize = (bubble1.radius + bubble2.radius) - collisionVector.length();
				if (emergeSize > Math.min(bubble1.radius, bubble2.radius) / 4.0 && bubble2.isEmergeable)
				{					
					this.removeChild(bubble2);
					this.bubbles.splice(bubbleIndex2, 1);
					bubbleIndex2--;
					
					bubble1.CollapseWith(bubble2, collisionVector);
					
					if (this.selectedBubble == bubble2) 
					{
						this.selectedBubble = bubble1;
					}
				}
				else 
				{
					let weight1 = bubble1.radius / (bubble1.radius + bubble2.radius);
					let emergeVector = new Victor(emergeSize, emergeSize);
					collisionVector.normalize().multiply(emergeVector);
					bubble1.velocity.add(collisionVector.clone().multiply(new Victor(1 - weight1, 1 - weight1)).invert());
					bubble2.velocity.add(collisionVector.clone().multiply(new Victor(weight1, weight1)));
				}
			}		
		}
		let linesToRemove = [];
		for (let lineIndex = 0; lineIndex < this.lines.length; lineIndex++)
		{
			let result = this.CheckLineCollision(bubble1, this.lines[lineIndex]);
			for (let toRemoveLineIndex = 0; toRemoveLineIndex < result.length; toRemoveLineIndex++)
			{
				linesToRemove.push(result[toRemoveLineIndex]);
			}		
		}	
		for (let lineIndex = 0; lineIndex < linesToRemove.length; lineIndex++)
		{
			let line = linesToRemove[lineIndex];
			let index = this.lines.indexOf(line);
			this.lines.splice(index, 1);
		}
		for (let itemIndex = 0; itemIndex < this.items.length; itemIndex++)
		{
			let item = this.items[itemIndex];
			if (!item.isCatched)
			{
				collisionVector.x = bubble1.position.x - item.position.x;
				collisionVector.y = bubble1.position.y - item.position.y;
				if (collisionVector.length() < bubble1.radius + item.radius)
				{
					if (bubble1.item == null && bubble1.canHandleItem)
					{
						item.CatchInBubble(bubble1);
						bubble1.item = item;
						bubble1.velocity.x = 0;
						bubble1.velocity.y = 0;
					}
					else
					{
						item.CollideWithBubble(bubble1);
					}
				}				
			}
		}	
	}
	
	for (let itemIndex = 0; itemIndex < this.items.length; itemIndex++)
	{
		var item = this.items[itemIndex];
		for (let lineIndex = 0; lineIndex < this.lines.length; lineIndex++)
		{
			this.CheckLineCollision(item, this.lines[lineIndex]);
		}
	}
	
	let viewportCenter = {
		x: -this.x + Wert.ViewportSize.current.width / 2, 
		y: -this.y + Wert.ViewportSize.current.height / 2
	};
	
	let diff = new Victor(
		this.hero.x - viewportCenter.x,
		this.hero.y - viewportCenter.y
	);
	
	let safeAreaRadius = 100;
	if (diff.length() > safeAreaRadius)
	{
		let moveOn = diff.length() - safeAreaRadius;
		diff.normalize().multiply(new Victor(moveOn, moveOn));
		this.x -= diff.x / 8;
		this.y -= diff.y / 8;
		
		this.x = Math.max(-this.map.size.width + Wert.ViewportSize.current.width, Math.min(0, this.x));
		this.y = Math.max(-this.map.size.height + Wert.ViewportSize.current.height, Math.min(0, this.y));
	}
	this.background.tilePosition.set(this.x / 4, this.y / 4);
	this.background.position.set(-this.x, -this.y);
	this.midground.tilePosition.set(this.x / 2, this.y / 2);
	this.midground.position.set(-this.x, -this.y);
	
	this.restartButton.x = -this.x + this.restartButton.width / 2 + 8;
	this.restartButton.y = -this.y + this.restartButton.height / 2 + 8;
	
	this.homeButton.x = -this.x + Wert.ViewportSize.min.width - this.homeButton.width * 1.3 - 8;
	this.homeButton.y = -this.y + this.homeButton.height / 2 + 8;
	
	this.soundButton.x = -this.x + Wert.ViewportSize.min.width - this.soundButton.width / 2 - 8;
	this.soundButton.y = -this.y + this.soundButton.height * 1.3 + 8;
};

Wert.StageGame.prototype.Resize = function(newSize) {	
	Wert.StageGame.super.Resize.call(this, newSize);
};

Wert.StageGame.prototype.OnDown = function(args) {	
	Wert.StageGame.super.OnDown.call(this, args);
	let x = this.mouse.x;
	let y = this.mouse.y;
	
	let mind = 9999;
	let selectedBubble = null;
	let distanceVector = new Victor(0, 0);
	for (let bubbleIndex = 0; bubbleIndex < this.bubbles.length; bubbleIndex++)
	{
		let bubble = this.bubbles[bubbleIndex];
		distanceVector.x = bubble.position.x - x;
		distanceVector.y = bubble.position.y - y;
		let distance = distanceVector.length();
		if (distance < mind && distance <= bubble.radius) {
			mind = distance;
			selectedBubble = bubble;
		}
	}
	
	this.selectedBubble = selectedBubble;
	if (this.selectedBubble == null) 
	{
		this.hero.state = Wert.BubbleHero.prototype.EState.Shooting;		
	}
};

Wert.StageGame.prototype.OnUp = function(args) {	
	Wert.StageGame.super.OnUp.call(this, args);
	this.selectedBubble = null;
	this.hero.state = Wert.BubbleHero.prototype.EState.Idle;
};

Wert.StageGame.prototype.OnMove = function(args) {	
	Wert.StageGame.super.OnMove.call(this, args);
};

Wert.StageGame.prototype.AddBubble = function(newBubble) {	
	this.addChild(newBubble);
	this.bubbles.push(newBubble);
};

Wert.StageGame.prototype.CheckLineCollision = function(bubble, line)
{
	let p1 = line.p1.clone();
	let p2 = line.p2.clone();
	let center = new Victor(bubble.position.x, bubble.position.y);
	let r = bubble.radius;
	let difference = p2.clone().subtract(p1);
	let fromCenterToP1 = p1.clone().subtract(center);
	let a = difference.dot(difference) ;
	let b = 2 * fromCenterToP1.dot(difference) ;
	let c = fromCenterToP1.dot(fromCenterToP1) - r * r 
	
	let discriminant = b * b - 4 * a * c;
	
	let toRemove = [];
	
	if( discriminant > 0 )
	{		
		discriminant = Math.sqrt(discriminant);
		let t1 = (-b - discriminant)/(2*a);	
		let t2 = (-b + discriminant)/(2*a);	
		let tm = (t1 + t2) / 2;
		let collisionPoint = null;
		if (tm >= 0 && tm <= 1)
		{
			collisionPoint = p1.clone();
			collisionPoint.x += difference.x * ((t2 + t1) / 2);
			collisionPoint.y += difference.y * ((t2 + t1) / 2);
		}
		else if(t1 >= 0 && t1 <= 1)
		{
			collisionPoint = p2;
		}
		else if( t2 >= 0 && t2 <= 1 )
		{
			collisionPoint = p1;	
		}
		if (collisionPoint != null)
		{
			let collisionVector = new Victor(0, 0);
			collisionVector.x = bubble.position.x - collisionPoint.x;
			collisionVector.y = bubble.position.y - collisionPoint.y;
			let emergeSize = bubble.radius - collisionVector.length();	
			let emergeVector = new Victor(emergeSize, emergeSize);
			collisionVector.normalize().multiply(emergeVector);
			bubble.position.x += collisionVector.x;
			bubble.position.y += collisionVector.y;	
			
			if (bubble instanceof Wert.Item)
			{
				bubble.Collide();
			}
			
			if (line.type === 'lock' && bubble.item instanceof Wert.Key)
			{
				let door = line.door;
				this.removeChild(door);
				this.removeChild(bubble.item);
				bubble.item = null;
				for (let lineIndex = 0; lineIndex < door.lines.length; lineIndex++)
				{
					let line = door.lines[lineIndex];
					line.type = null;
					toRemove.push(line);
				}
			}
		}
	}
	return toRemove;
}

Wert.StageGame.prototype.FillStage = function(map)
{
	let terrainLayer = null;
	let collisionLayer = null;
	let objectsLayer = null;
	let imagesLayer = null;
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	let layers = map.layers;
	for (let layerIndex = 0; layerIndex < layers.length; layerIndex++)
	{
		let layer = layers[layerIndex];
		switch(layer.name)
		{
			case "Terrain":
				terrainLayer = layer;
				break;
			case "Collisions":
				collisionLayer = layer;
				break;
			case "Objects":
				objectsLayer = layer;
				break;
			case "Tiles":
				imagesLayer = layer;
				break;
		}
	}
	if (objectsLayer != null)
	{
		let objects = objectsLayer.objects;
		for (let objectIndex = 0; objectIndex < objects.length; objectIndex++)
		{
			let object = objects[objectIndex];
			let stageObject = null;
			let layer = this;
			switch (object.type)
			{
				case "hero":
					stageObject = new Wert.BubbleHero();
					stageObject.SetRadius(object.width / 2);
					this.bubbles.push(stageObject);
					this.hero = stageObject;
					stageObject.x = object.x + object.width / 2;
					stageObject.y = object.y + object.height / 2;
					break;
				case "mom":
					stageObject = new Wert.BubbleMom();
					stageObject.SetRadius(object.width / 2);
					this.bubbles.push(stageObject);
					stageObject.x = object.x + object.width / 2;
					stageObject.y = object.y + object.height / 2;
					break;
				case "child":
					stageObject = new Wert.BubbleChild();
					stageObject.SetRadius(object.width / 2);
					this.bubbles.push(stageObject);
					stageObject.x = object.x + object.width / 2;
					stageObject.y = object.y + object.height / 2;
					break;
				case "exit":
					stageObject = new Wert.Exit(object);
					stageObject.x = object.x;
					stageObject.y = object.y;
					break;
				case "drain":
					stageObject = new Wert.Drain(object);
					stageObject.x = object.x;
					stageObject.y = object.y;
					break;
				case "bubble":
					stageObject = new Wert.Bubble();
					stageObject.x = object.x + object.width / 2;
					stageObject.y = object.y + object.height / 2;
					stageObject.SetRadius(object.width / 2);
					this.bubbles.push(stageObject);
					break;
				case "key":
					stageObject = new Wert.Key();
					stageObject.x = object.x;
					stageObject.y = object.y;
					this.items.push(stageObject);
					break;
				case "lock":
					stageObject = new Wert.Door(object);
					this.items.push(stageObject);
					for (let lineIndex = 0; lineIndex < stageObject.lines.length; lineIndex++)
					{
						this.lines.push(stageObject.lines[lineIndex]);
					}
					break;
				case "fish":
					stageObject = new Wert.EnemyFish(object);
					stageObject.x = object.x;
					stageObject.y = object.y;
					this.items.push(stageObject);
					break;
				case "mine":
					stageObject = new Wert.EnemyMine(object);
					stageObject.x = object.x;
					stageObject.y = object.y;
					this.items.push(stageObject);
					break;
				case "text":
					let text = object.text;
					stageObject = new PIXI.Text(text.text, {
						fontFamily : text.fontfamily ? text.fontfamily : 'Arial', 
						fontSize: text.pixelsize ? text.pixelsize : 16,
						fill: PIXI.utils.string2hex(text.color ? text.color : "#000000"),
						wordWrap: text.wrap,
						wordWrapWidth: object.width,
						align: text.halign ? text.halign : 'left'
					});
					stageObject.position.set(object.x, object.y);
					
					layer = this.textLayer;
					break;
			}
			if (stageObject != null)
			{
				layer.addChild(stageObject);
			}
		}		
	}
	if (terrainLayer != null)
	{	
		let objects = terrainLayer.objects;
		for (let objectIndex = 0; objectIndex < objects.length; objectIndex++)
		{
			let object = objects[objectIndex];
			let polygon = object.polygon;
			if (polygon)
			{
				//create a polygon
				var graphics = new PIXI.Graphics();
				graphics.beginFill(0x0D1A20);
				
				let minx = 99999;
				let miny = 99999;
				let maxx = 0;
				let maxy = 0;
				
				graphics.moveTo(polygon[0].x, polygon[0].y);					
				for (let vertexIndex = 1; vertexIndex < polygon.length; vertexIndex++)
				{
					let vertex = polygon[vertexIndex];									
					graphics.lineTo(vertex.x, vertex.y);					
					if (vertex.x < minx)
					{
						minx = vertex.x;
					}
					if (vertex.y < miny)
					{
						miny = vertex.y;
					}
					if (vertex.x > maxx)
					{
						maxx = vertex.x;
					}
					if (vertex.y > maxy)
					{
						maxy = vertex.y;
					}
				}
				
				graphics.endFill();
				
				graphics.position.x = object.x;
				graphics.position.y = object.y;
				
				let layer = this;
				if (object.type == 'inner')
				{
					layer = this.innerLayer;
				}
				layer.addChild(graphics);
			}
		}
	}
	if (imagesLayer != null)
	{
		let tiles = PIXI.Loader.shared.resources["Tileset"].data.tiles;
		let objects = imagesLayer.objects;
		for (let objectIndex = objects.length - 1; objectIndex >= 0; objectIndex--)
		{
			let object = objects[objectIndex];
			let textureName = null;
			for (let tileIndex = 0; tileIndex < tiles.length; tileIndex++)
			{
				let tile = tiles[tileIndex];
				if (object.gid - 1 === tile.id)
				{
					textureName = tile.image;
					textureName = textureName.substr(textureName.lastIndexOf('/') + 1);
					textureName = textureName.substr(0, textureName.lastIndexOf('.'));
					break;
				}
			}		
			
			let sprite = new PIXI.Sprite(sheet.textures['tiles/' + textureName]);
			sprite.position.set(object.x, object.y);
			sprite.anchor.set(0, 1);
			this.addChild(sprite);
		}
	}
	if (collisionLayer != null)
	{	
		let objects = collisionLayer.objects;
		for (let objectIndex = 0; objectIndex < objects.length; objectIndex++)
		{
			let object = objects[objectIndex];
			let polygon = object.polygon;
			if (polygon)
			{				
				for (let vertexIndex = 0; vertexIndex < polygon.length; vertexIndex++)
				{
					let vertex = polygon[vertexIndex];					
					let nextVertex = polygon[(vertexIndex + 1) % polygon.length];
					let line = {
						p1: new Victor(
							object.x + vertex.x,
							object.y + vertex.y
						),
						p2: new Victor(
							object.x + nextVertex.x,
							object.y + nextVertex.y
						)
					};
					
					this.lines.push(line);
				}
			}
		}
	}
};

Wert.StageGame.prototype.GotoNextLevel = function()
{
	let storedLevel = null;
	try {
		storedLevel = window.localStorage.getItem('avaiableLevel');
	}catch{}
	let nextLevelNumber = this.levelNumber + 1;
	let maxLevel = storedLevel ? Math.max(parseInt(storedLevel), nextLevelNumber) : nextLevelNumber;
	try
	{
		window.localStorage.setItem('avaiableLevel', maxLevel);
	}catch{
		Wert.StageLevelSelect.prototype.avaiableLevel = maxLevel;
	}
	Wert.SwitchStage(new Wert.StageGame(this.levelNumber + 1));
};

Wert.StageGame.prototype.CheckLineIntersection = function(line)
{
	for (let lineIndex = 0; lineIndex < this.lines.length; lineIndex++)
	{
		let res = Wert.LineIntersection(this.lines[lineIndex], line);
		if (res != null)
		{
			return res;
		}
	}
	return null;
}
Wert.Drain = function(data) {
	Wert.Drain.super.constructor.call(this);
	
	this.bubbles = [];
	this.size = {
		width: data.width, 
		height: data.height
	};
	if (data.properties)
	{
		for (let propertyIndex = 0; propertyIndex < data.properties.length; propertyIndex++)
		{
			let property = data.properties[propertyIndex];
			switch (property.name)
			{
				case "direction":
					this.direction = Math.PI * property.value / 180.0;
					break;
				case "power":
					this.power = property.value;
					break;
			}
		}
	}	
	this.bubblesCount = Math.min(100, (this.size.width * this.size.height) / 1024);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	for (var index = 0; index < this.bubblesCount; index++)
	{
		let bubble = new PIXI.Sprite(sheet.textures["small/bubble_" + Math.floor(1 + Math.random() * 7)]);
		bubble.position.x = Math.random() * this.size.width;
		bubble.position.y = Math.random() * this.size.height;
		bubble.anchor.set(0.5);
		bubble.alpha = 0;
		bubble.scale.x = bubble.scale.y = Math.random();
		
		this.addChild(bubble);
		this.bubbles.push(bubble);
	}
	
	this.dx = this.power * Math.cos(this.direction);
	this.dy = this.power * Math.sin(this.direction);
}

Extend(Wert.Drain, PIXI.Container);

Wert.Drain.prototype.bubbles = null;
Wert.Drain.prototype.size = null;
Wert.Drain.prototype.direction = 0;
Wert.Drain.prototype.power = 1.5;
Wert.Drain.prototype.dx = 0;
Wert.Drain.prototype.dy = 0;

Wert.Drain.prototype.Update = function()
{
	Wert.Drain.super.Update.call(this);
	
	for (let bubbleIndex = 0; bubbleIndex < this.bubblesCount; bubbleIndex++)
	{
		let bubble = this.bubbles[bubbleIndex];
		bubble.x += this.dx;
		bubble.y += this.dy;
		
		bubble.alpha += (1 - bubble.alpha) / 8.0;
		bubble.scale.x = bubble.scale.y = bubble.scale.x - 0.01;
		
		if (bubble.scale.x < 0.01)
		{
			bubble.position.x = Math.random() * this.size.width;
			bubble.position.y = Math.random() * this.size.height;	
			bubble.scale.x = bubble.scale.y = 1;
			bubble.alpha = 0;
		}
	}
	
	let x2 = this.x + this.size.width;
	let y2 = this.y + this.size.height;
	for (let bubbleIndex = 0; bubbleIndex < Wert.CurrentStage.bubbles.length; bubbleIndex++)
	{
		let bubble = Wert.CurrentStage.bubbles[bubbleIndex];
		
		if (bubble.x >= this.x && bubble.x <= x2 && bubble.y >= this.y && bubble.y <= y2)
		{
			bubble.velocity.x += this.dx;
			bubble.velocity.y += this.dy;			
		}
	}
}
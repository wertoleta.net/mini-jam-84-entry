Wert.StageLevelSelect = function() {
	Wert.StageLevelSelect.super.constructor.call(this, 0);
	
	this.map = PIXI.Loader.shared.resources["LevelSelect"].data;
	this.map.size = {
		width: Math.max(this.map.width * this.map.tilewidth, Wert.ViewportSize.min.width),
		height: Math.max(this.map.height * this.map.tileheight, Wert.ViewportSize.min.height),
	};
	
	this.hero = new Wert.BubbleHero();
	
	let storedLevel = null;
	try {
		storedLevel = window.localStorage.getItem('avaiableLevel');
	} catch{}
	
	this.avaiableLevel = storedLevel ? parseInt(storedLevel) : this.avaiableLevel;
}

Extend(Wert.StageLevelSelect, Wert.StageGame);

Wert.StageLevelSelect.prototype.avaiableLevel = 1;

Wert.StageLevelSelect.prototype.Create = function() {
	Wert.StageLevelSelect.super.Create.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	for (let buttonIndex = 0; buttonIndex < 5; buttonIndex++)
	{
		let button = new Wert.Button(buttonIndex < this.avaiableLevel ? sheet.textures['buttons/empty'] : sheet.textures['buttons/locked'], function(){
			Wert.SwitchStage(new Wert.StageGame(this.level));
		});
		button.level = buttonIndex + 1;
		button.active = buttonIndex < this.avaiableLevel;
		button.normalScale = 0.5;
		button.position.set(100 + (600 / 4) * buttonIndex, 272);
	
		if (button.active)
		{
			let text = new PIXI.Text((buttonIndex + 1).toString(), {
				fontSize: 128,
				fill: 0xffffff
			});
			text.anchor.set(0.5);
			text.position.set(0, 10);
			button.addChild(text);
		}
		this.addChild(button);
	}
	
	this.removeChild(this.restartButton);
};

Wert.StageLevelSelect.prototype.Update = function() {
	Wert.StageLevelSelect.super.Update.call(this);
};

Wert.StageLevelSelect.prototype.Resize = function(newSize) {	
	Wert.StageLevelSelect.super.Resize.call(this, newSize);
};
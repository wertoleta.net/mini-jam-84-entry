Wert.Exit = function(data) {
	Wert.Exit.super.constructor.call(this);
	
	this.size = {
		width: data.width, 
		height: data.height
	};
}

Extend(Wert.Exit, PIXI.Container);

Wert.Exit.prototype.size = null;

Wert.Exit.prototype.Update = function()
{
	Wert.Exit.super.Update.call(this);
	
	let bubble = Wert.CurrentStage.hero;
	
	let x2 = this.x + this.size.width;
	let y2 = this.y + this.size.height;
	if (bubble.x >= this.x && bubble.x <= x2 && bubble.y >= this.y && bubble.y <= y2)
	{
		Wert.CurrentStage.GotoNextLevel();
	}
}
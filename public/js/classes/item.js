Wert.Item = function() {
	Wert.Item.super.constructor.call(this);
}

Extend(Wert.Item, PIXI.Container);

Wert.Item.prototype.weight = 2;
Wert.Item.prototype.radius = 16;
Wert.Item.prototype.isCatched = false;
Wert.Item.prototype.containerBubble = null;


Wert.Item.prototype.Update = function() {
	Wert.Item.super.Update.call(this);
	
	this.position.y += this.weight;
};

Wert.Item.prototype.CatchInBubble = function(bubble)
{
	this.isCatched = true;
	this.containerBubble = bubble;
}

Wert.Item.prototype.FreeFromBubble = function()
{
	this.isCatched = false;
	this.containerBubble = null;
	this.skew.x = 0;
	this.skew.y = 0;
};

Wert.Item.prototype.Collide = function()
{
};

Wert.Item.prototype.CollideWithBubble = function()
{
};
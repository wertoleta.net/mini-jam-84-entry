Wert.StageMainMenu = function() {
	Wert.StageMainMenu.super.constructor.call(this, 0);
	
	this.map = PIXI.Loader.shared.resources["MainMenu"].data;
	this.map.size = {
		width: Math.max(this.map.width * this.map.tilewidth, Wert.ViewportSize.min.width),
		height: Math.max(this.map.height * this.map.tileheight, Wert.ViewportSize.min.height),
	};
	
	this.hero = new Wert.BubbleHero();
}

Extend(Wert.StageMainMenu, Wert.StageGame);

Wert.StageMainMenu.prototype.playButton = null;

Wert.StageMainMenu.prototype.Create = function() {
	Wert.StageMainMenu.super.Create.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	this.playButton = new Wert.Button(sheet.textures['buttons/play'], function(){
		Wert.SwitchStage(new Wert.StageLevelSelect());
		if (!Wert.SoundPlayed)
		{
			PIXI.Loader.shared.resources["BackgroundMusic"].data.play();
			Wert.SoundPlayed = true;
			Wert.IsMuted = false;
		}
	});
	this.playButton.position.set(Wert.ViewportSize.min.width / 2, 272);
	this.addChild(this.playButton);
	
	this.removeChild(this.restartButton);
	this.removeChild(this.homeButton);
};

Wert.StageMainMenu.prototype.Update = function() {
	Wert.StageMainMenu.super.Update.call(this);
	//this.background.tilePosition.set(this.mouse.x / 16, this.mouse.y / 16);
	//this.midground.tilePosition.set(this.mouse.x / 8, this.mouse.y / 8);
};

Wert.StageMainMenu.prototype.Resize = function(newSize) {	
	Wert.StageMainMenu.super.Resize.call(this, newSize);
	
	//this.button1.x = Wert.Center.x - newSize.width / 2 + this.buttonOffset;
	//this.button1.y = Wert.Center.y - newSize.height / 2 + this.buttonOffset;
	//
	//this.button2.x = Wert.Center.x + newSize.width / 2 - this.button2.width - this.buttonOffset;
	//this.button2.y = Wert.Center.y + newSize.height / 2 - this.button2.height - this.buttonOffset;
};
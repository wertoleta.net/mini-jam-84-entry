Wert.EnemyFish = function() {
	Wert.EnemyFish.super.constructor.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	this.sprite = new PIXI.AnimatedSprite(sheet.animations['fish']);
	this.sprite.anchor.set(0.5);
	this.sprite.play();
	this.sprite.scale.x = -1;
	this.sprite.animationSpeed = 0.1;
	this.scale.set(2);
	this.addChild(this.sprite);
	
	this.radius = 16;
}

Extend(Wert.EnemyFish, Wert.Enemy);

Wert.EnemyFish.prototype.EState = {
	Live: 0,
	Dead: 1,
	InBubble: 2
};

Wert.EnemyFish.prototype.sprite = null;
Wert.EnemyFish.prototype.speed = 2;
Wert.EnemyFish.prototype.maxHealth = 120;
Wert.EnemyFish.prototype.health = Wert.EnemyFish.prototype.maxHealth;
Wert.EnemyFish.prototype.state = Wert.EnemyFish.prototype.EState.Live;

Wert.EnemyFish.prototype.Update = function() {
	Wert.EnemyFish.super.Update.call(this);
	
	switch (this.state)
	{
		case Wert.EnemyFish.prototype.EState.Live:
			let hero = Wert.CurrentStage.hero;
			let visionLine = {
				p1: this.position,
				p2: hero.position
			};
			
			let freeLine = hero.isAlive && Wert.CurrentStage.CheckLineIntersection(visionLine) == null;
			if (freeLine)
			{
				let direction = new Victor(visionLine.p2.x - visionLine.p1.x, visionLine.p2.y - visionLine.p1.y);
				let a = direction.angle();
				this.position.x += this.speed * Math.cos(a);
				this.position.y += this.speed * Math.sin(a);
				
				if (direction.length() < hero.radius + this.radius)
				{
					hero.Kill();
				}
				
				if (visionLine.p1.x > visionLine.p2.x)
				{
					this.sprite.scale.x = -1;
				}
				else
				{
					this.sprite.scale.x = 1;
				}
			}
			else
			{
				this.x += this.speed * this.sprite.scale.x;
			}
			if (this.containerBubble != null && this.containerBubble.radius >= this.radius)
			{
				this.state = Wert.EnemyFish.prototype.EState.InBubble;
			}
			this.health = Math.min(this.maxHealth, this.health + 1);
			break;
		case Wert.EnemyFish.prototype.EState.Dead:
			break;
		case Wert.EnemyFish.prototype.EState.InBubble:
			this.health = Math.max(0, this.health - 1);
			if (this.health <= 0)
			{
				this.state = Wert.EnemyFish.prototype.EState.Dead;	
				this.sprite.scale.y = -1;			
				this.weight = 2;
			}
			else if (this.containerBubble == null)
			{
				this.state = Wert.EnemyFish.prototype.EState.Live;
			}
			break;
	}
	this.sprite.animationSpeed = 0.1 * this.health / this.maxHealth;
};

Wert.EnemyFish.prototype.Collide = function(){
	if (this.state == Wert.EnemyFish.prototype.EState.Live)
	{
		this.sprite.scale.x *= -1;
	}
};
Wert.BubbleHero = function() {
	Wert.BubbleHero.super.constructor.call(this);
	
	this.face = new PIXI.Sprite(this.SmileFaceTexture);
	this.face.anchor.set(0.5);
	
	this.eyelids = new PIXI.Sprite(this.EyelidsTexture);
	this.eyelids.visible = false;
	this.eyelids.anchor.set(0.5);
	
	this.eyelidsTimer = 0;
	this.shootCooldown = 0;
	this.state = Wert.BubbleHero.prototype.EState.Idle;
	this.upSpeed = 0;
	this.canHandleItem = false;
	this.isEmergeable = false;
	
	this.SetArea(this.MediumSizeArea);
	
	this.addChild(this.face);
	this.addChild(this.eyelids);
};

Extend(Wert.BubbleHero, Wert.Bubble);

Wert.BubbleHero.prototype.SmileFaceTexture = null;
Wert.BubbleHero.prototype.BigSmileFaceTexture = null;
Wert.BubbleHero.prototype.SurprisedFaceTexture = null;
Wert.BubbleHero.prototype.AngryFaceTexture = null;
Wert.BubbleHero.prototype.AshamedFaceTexture = null;
Wert.BubbleHero.prototype.EyelidsTexture = null;

Wert.BubbleHero.prototype.MinimalSizeArea = 5000;
Wert.BubbleHero.prototype.HugeSizeArea = 15000;
Wert.BubbleHero.prototype.MediumSizeArea = (Wert.BubbleHero.prototype.HugeSizeArea + Wert.BubbleHero.prototype.MinimalSizeArea) / 2;

Wert.BubbleHero.prototype.state = null;
Wert.BubbleHero.prototype.shootCooldown = 0;

Wert.BubbleHero.prototype.eyelids = null;
Wert.BubbleHero.prototype.eyelidsTimer = 0;
Wert.BubbleHero.prototype.face = null;
Wert.BubbleHero.prototype.isAlive = true;
Wert.BubbleHero.prototype.deathTimer = 0;
Wert.BubbleHero.prototype.bulletArea = 1000;

Wert.BubbleHero.prototype.EState = {
	Idle: 0,
	Shooting: 1
};


Wert.BubbleHero.prototype.Update = function(){
	Wert.BubbleHero.super.Update.call(this);
	
	if (!this.isAlive)
	{
		this.deathTimer++;
		if (this.deathTimer > 120)
		{
			Wert.SwitchStage(new Wert.StageGame(Wert.CurrentStage.levelNumber));
		}
		return;
	}
	
	this.shootCooldown = Math.max(0, this.shootCooldown - 1);
	switch (this.state)
	{
		case Wert.BubbleHero.prototype.EState.Idle:
			if (this.speed.length() > 1) 
			{
				this.face.texture = this.SurprisedFaceTexture;
			}
			else 
			{
				this.face.texture = (this.area > this.HugeSizeArea ? this.BigSmileFaceTexture : (this.area > this.MediumSizeArea ? this.SmileFaceTexture : this.AshamedFaceTexture));
			}
			break;
		case Wert.BubbleHero.prototype.EState.Shooting:		
			this.face.texture = this.area - this.bulletArea < this.MinimalSizeArea ? this.ConfusedFaceTexture : this.AngryFaceTexture;
			if (this.shootCooldown <= 0) 
			{
				this.Shoot();
			}
			break;
	}
	
	this.eyelidsTimer--;
	if (this.eyelidsTimer <= 0) 
	{
		this.eyelidsTimer = this.eyelids.visible ? 60 + Math.floor(Math.random() * 30) : 10;
		this.eyelids.visible = !this.eyelids.visible;
	}
};

Wert.BubbleHero.prototype.Shoot = function()
{	
	if (this.area - this.bulletArea < this.MinimalSizeArea) 
	{
		return;
	}
	
	this.SetArea(this.area - this.bulletArea);
	this.shootCooldown = 10;
	
	let bullet = new Wert.Bubble();
	bullet.SetArea(this.bulletArea);

	let shootSpeed = 2000 + 6000 * (this.bulletArea / 1000);
	
	let angle = new Victor(Wert.CurrentStage.mouse.x - this.position.x, Wert.CurrentStage.mouse.y - this.position.y).angle();
	let distance = this.radius + bullet.radius;
	bullet.position.x = this.position.x + Math.cos(angle) * distance;
	bullet.position.y = this.position.y + Math.sin(angle) * distance;
	bullet.velocity.x = Math.cos(angle) * shootSpeed;
	bullet.velocity.y = Math.sin(angle) * shootSpeed;
	bullet.scale.x = bullet.scale.y = bullet.radiusScale;
	Wert.CurrentStage.AddBubble(bullet);
	
	if (!Wert.IsMuted)
	{
		PIXI.Loader.shared.resources["Fire"].data.currentTime = 0;
		PIXI.Loader.shared.resources["Fire"].data.play();
	}
};	

Wert.BubbleHero.prototype.Kill = function(){
	if (!this.isAlive)
	{
		return;
	}
	this.isAlive = false;
	this.body.visible = this.face.visible = this.eyelids.visible = false;
	Wert.CurrentStage.bubbles.splice(Wert.CurrentStage.bubbles.indexOf(this), 1);
	
	while (this.area > 0)
	{
		let bubble = new Wert.Bubble();
		let a = Math.random() * Math.PI * 2;
		let d = Math.random() * this.radius;
		let dx = d * Math.cos(a);
		let dy = d * Math.sin(a);
		let area = 300 + Math.random() * 700;
		bubble.position.set(this.x + dx, this.y + dy * Math.sin(a));
		bubble.SetArea(area);
		bubble.velocity.x = dx * 100;
		bubble.velocity.y = dy * 100;
		bubble.isEmergeable = false;
		Wert.CurrentStage.AddBubble(bubble);		
		this.area -= area;
	}
	if (!Wert.IsMuted)
	{
		PIXI.Loader.shared.resources["Death"].data.currentTime = 0;
		PIXI.Loader.shared.resources["Death"].data.play();
	}
};
function Start() {
	//Create a Pixi Application 
	Wert.App = new PIXI.Application({width: 256, height: 256});

	//Add the canvas that Pixi automatically created for you to the HTML document
	document.body.appendChild(Wert.App.view);

	Wert.SwitchStage(new Wert.StagePreloader());

	window.addEventListener("resize", DoResize);
	Wert.Resize2(Wert.ViewportSize.min);
	DoResize();

	Wert.App.ticker.add(function (time) {
		 Wert.Update(time);
	});
}

//Скейл и центровка "Игры"
function DoResize() 
{
	Wert.Resize({
		width: window.innerWidth,
		height: window.innerHeight
	});
}

window.onload = Start;
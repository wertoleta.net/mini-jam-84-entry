<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename>../public/img/atlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../public/img/atlas.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">images/background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,64,144,128</rect>
                <key>scale9Paddings</key>
                <rect>72,64,144,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/bubble_1.png</key>
            <key type="filename">images/bubble_2.png</key>
            <key type="filename">images/bubble_3.png</key>
            <key type="filename">images/bubble_4.png</key>
            <key type="filename">images/bubble_5.png</key>
            <key type="filename">images/bubble_6.png</key>
            <key type="filename">images/faces/smile_mom.png</key>
            <key type="filename">images/faces/surprised_mom.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,128,256,256</rect>
                <key>scale9Paddings</key>
                <rect>128,128,256,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/bubble_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>120,120,239,239</rect>
                <key>scale9Paddings</key>
                <rect>120,120,239,239</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/buttons/empty.png</key>
            <key type="filename">images/buttons/locked.png</key>
            <key type="filename">images/buttons/restart.png</key>
            <key type="filename">images/buttons/sound-off.png</key>
            <key type="filename">images/buttons/sound-on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,43,85,87</rect>
                <key>scale9Paddings</key>
                <rect>43,43,85,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/buttons/home.png</key>
            <key type="filename">images/buttons/play.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,43,86,87</rect>
                <key>scale9Paddings</key>
                <rect>43,43,86,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/faces/angry.png</key>
            <key type="filename">images/faces/ashamed.png</key>
            <key type="filename">images/faces/confused.png</key>
            <key type="filename">images/faces/expresionless.png</key>
            <key type="filename">images/faces/expresionless_blink.png</key>
            <key type="filename">images/faces/smile.png</key>
            <key type="filename">images/faces/smile_big.png</key>
            <key type="filename">images/faces/smile_child.png</key>
            <key type="filename">images/faces/surprised.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,64,128,128</rect>
                <key>scale9Paddings</key>
                <rect>64,64,128,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/fish_1.png</key>
            <key type="filename">images/fish_2.png</key>
            <key type="filename">images/fish_3.png</key>
            <key type="filename">images/fish_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,5,19,10</rect>
                <key>scale9Paddings</key>
                <rect>10,5,19,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/key-blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,7,7,14</rect>
                <key>scale9Paddings</key>
                <rect>3,7,7,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/lock-blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,16,128,32</rect>
                <key>scale9Paddings</key>
                <rect>64,16,128,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/lock-blue2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,64,32,128</rect>
                <key>scale9Paddings</key>
                <rect>16,64,32,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/midground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>240,128,480,256</rect>
                <key>scale9Paddings</key>
                <rect>240,128,480,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/mine-big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,35,35</rect>
                <key>scale9Paddings</key>
                <rect>17,17,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/small/bubble_1.png</key>
            <key type="filename">images/small/bubble_2.png</key>
            <key type="filename">images/small/bubble_3.png</key>
            <key type="filename">images/small/bubble_4.png</key>
            <key type="filename">images/small/bubble_5.png</key>
            <key type="filename">images/small/bubble_6.png</key>
            <key type="filename">images/small/bubble_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,25,43,49</rect>
                <key>scale9Paddings</key>
                <rect>21,25,43,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,17,26,33</rect>
                <key>scale9Paddings</key>
                <rect>13,17,26,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop2.png</key>
            <key type="filename">images/tiles/prop7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,51,99,103</rect>
                <key>scale9Paddings</key>
                <rect>50,51,99,103</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,58,46,115</rect>
                <key>scale9Paddings</key>
                <rect>23,58,46,115</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,58,60,115</rect>
                <key>scale9Paddings</key>
                <rect>30,58,60,115</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,47,123,93</rect>
                <key>scale9Paddings</key>
                <rect>61,47,123,93</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,35,57,70</rect>
                <key>scale9Paddings</key>
                <rect>29,35,57,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,13,23,25</rect>
                <key>scale9Paddings</key>
                <rect>12,13,23,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/prop9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,14,24,28</rect>
                <key>scale9Paddings</key>
                <rect>12,14,24,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-bottom1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,17,88,34</rect>
                <key>scale9Paddings</key>
                <rect>44,17,88,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-bottom2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,14,40,28</rect>
                <key>scale9Paddings</key>
                <rect>20,14,40,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-bottom3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,15,40,29</rect>
                <key>scale9Paddings</key>
                <rect>20,15,40,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-island.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,47,89,94</rect>
                <key>scale9Paddings</key>
                <rect>45,47,89,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-left-bottom.png</key>
            <key type="filename">images/tiles/rock-left-top.png</key>
            <key type="filename">images/tiles/rock-right-bottom.png</key>
            <key type="filename">images/tiles/rock-right-top.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,29,56,58</rect>
                <key>scale9Paddings</key>
                <rect>28,29,56,58</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-left-bottom2.png</key>
            <key type="filename">images/tiles/rock-left-top2.png</key>
            <key type="filename">images/tiles/rock-right-bottom2.png</key>
            <key type="filename">images/tiles/rock-right-top2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-left-bottom3.png</key>
            <key type="filename">images/tiles/rock-left-top3.png</key>
            <key type="filename">images/tiles/rock-right-bottom3.png</key>
            <key type="filename">images/tiles/rock-right-top3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,14,26,28</rect>
                <key>scale9Paddings</key>
                <rect>13,14,26,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-left1.png</key>
            <key type="filename">images/tiles/rock-right1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,20,23,40</rect>
                <key>scale9Paddings</key>
                <rect>12,20,23,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-left2.png</key>
            <key type="filename">images/tiles/rock-right2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,20,31,40</rect>
                <key>scale9Paddings</key>
                <rect>15,20,31,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-top1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,16,40,31</rect>
                <key>scale9Paddings</key>
                <rect>20,16,40,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/tiles/rock-top2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,16,40,32</rect>
                <key>scale9Paddings</key>
                <rect>20,16,40,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>images</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
